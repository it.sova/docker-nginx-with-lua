#!/usr/bin/env bash
NGINX_IMAGE=nginx:with-lua

docker build . -t ${NGINX_IMAGE} && \
clear && \
docker run -p 80:80 -v $(pwd)/conf:/opt/nginx/conf ${NGINX_IMAGE}