ARG NGINX_VERSION=1.19.3
ARG NGINX_PREFIX=/opt/nginx
ARG NGINX_LUA=0.10.20
ARG LUA_JIT_VERSION=2.1-20210510
ARG LUA_RESTY_CORE_VERSION=0.1.22
ARG LUA_RESTY_LRUCACHE_VERSION=0.11
ARG NDK_VERSION=0.3.1

FROM debian:9 as generic-builder
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update && \
    apt install -y wget tar gcc make libpcre3 libpcre3-dev zlib1g-dev

# LUA JIT Builder
FROM generic-builder as luajit-builder
ARG LUA_JIT_VERSION

RUN wget "https://github.com/openresty/luajit2/archive/refs/tags/v${LUA_JIT_VERSION}.tar.gz" && \
    tar -zxvf v${LUA_JIT_VERSION}.tar.gz && \
    cd luajit2-${LUA_JIT_VERSION} && \
    make -j$(nproc) && make install

# LUA Resty Core builder
FROM generic-builder as lua-resty-core-builder
ARG LUA_RESTY_CORE_VERSION
ARG LUA_RESTY_LRUCACHE_VERSION

RUN wget "https://github.com/openresty/lua-resty-core/archive/refs/tags/v${LUA_RESTY_CORE_VERSION}.tar.gz" && \
    wget "https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v${LUA_RESTY_LRUCACHE_VERSION}.tar.gz" && \
    cat *.tar.gz | tar -zxvf - -i && \
    cd /lua-resty-core-${LUA_RESTY_CORE_VERSION} && make install && \
    cd /lua-resty-lrucache-${LUA_RESTY_LRUCACHE_VERSION} && make install

# NGINX Builder
FROM generic-builder as nginx-builder
ARG NGINX_PREFIX
ARG NGINX_VERSION
ARG NDK_VERSION
ARG NGINX_LUA

COPY --from=luajit-builder /usr/local/include/luajit-2.1 /luajit/include
COPY --from=luajit-builder /usr/local/lib /luajit/lib

ARG LUAJIT_LIB=/luajit/lib
ARG LUAJIT_INC=/luajit/include

RUN wget "http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz" && \
    wget "https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v${NDK_VERSION}.tar.gz" && \
    wget "https://github.com/openresty/lua-nginx-module/archive/refs/tags/v${NGINX_LUA}.tar.gz" && \
    cat *.tar.gz | tar -zxvf - -i

RUN cd nginx-${NGINX_VERSION} && \
    ./configure \
    --prefix=${NGINX_PREFIX} \
    --with-ld-opt="-Wl,-rpath,${NGINX_PREFIX}/lib" \
    --add-module=/ngx_devel_kit-${NDK_VERSION} \
    --add-module=/lua-nginx-module-${NGINX_LUA} && \
    make -j$(nproc) && make install;

# Final image
FROM debian:9
ARG NGINX_PREFIX
# No one loves sh >_>
SHELL ["/bin/bash", "-c"]
WORKDIR ${NGINX_PREFIX}

COPY --from=nginx-builder /opt/nginx/sbin/nginx ./sbin/
COPY --from=luajit-builder /usr/local/lib/libluajit-5.1.so.2 ./lib/
COPY --from=lua-resty-core-builder /usr/local/lib/lua/resty ./resty

RUN mkdir -p ${NGINX_PREFIX}/{logs,conf,modules} && \
    touch ${NGINX_PREFIX}/logs/access.log && \
    touch ${NGINX_PREFIX}/logs/error.log && \
    # Cuz we want to use docker logs ^_^
    ln -sf /dev/stdout ${NGINX_PREFIX}/logs/access.log && \
    ln -sf /dev/stderr ${NGINX_PREFIX}/logs/error.log && \
    chmod +x ./sbin/nginx;

EXPOSE 80 443

CMD ["./sbin/nginx", "-g", "daemon off;"]
