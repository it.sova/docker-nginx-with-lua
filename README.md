## NGINX with LUA module
### It works xD
NGINX response generated using lua `os.date()` function:
```shell
1-4 on  master [?]
❯ curl localhost
Hi There! <br> Current date ( by LUA ): Tue Oct  5 20:52:13 2021

1-4 on  master [?]
❯ curl localhost
Hi There! <br> Current date ( by LUA ): Tue Oct  5 20:52:15 2021

1-4 on  master [?]
❯ curl localhost
Hi There! <br> Current date ( by LUA ): Tue Oct  5 20:52:15 2021
```
We even have logs in `stdout`! Amazing!
```
Successfully built f429f44e3f32
Successfully tagged nginx:with-lua
10.254.1.1 - - [05/Oct/2021:20:52:13 +0000] "GET / HTTP/1.1" 200 76 "-" "curl/7.74.0"
10.254.1.1 - - [05/Oct/2021:20:52:15 +0000] "GET / HTTP/1.1" 200 76 "-" "curl/7.74.0"
10.254.1.1 - - [05/Oct/2021:20:52:15 +0000] "GET / HTTP/1.1" 200 76 "-" "curl/7.74.0"
```
And it's less than 1TB in size! Production-ready solution!
```
❯ docker images
REPOSITORY   TAG        IMAGE ID       CREATED          SIZE
nginx        with-lua   f429f44e3f32   12 minutes ago   124MB
```